# Define the exports.
export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export ZSH=$HOME/.oh-my-zsh
export ANDROID_HOME=$HOME/Library/Android/sdk
export JAVA_HOME=/Library/Java/JavaVirtualMachines/openjdk-14.0.2.jdk/Contents/Home
export NVM_DIR=/usr/local/opt/nvm

export PATH=/usr/local/bin:$PATH
export PATH=/usr/local/sbin:$PATH
export PATH=/usr/local/opt/java/bin:$PATH
export PATH=/usr/local/opt/mysql@8.0/bin:$PATH
export PATH=/usr/local/opt/ruby/bin:$PATH
export PATH=$HOME/.composer/vendor/bin:$PATH
export PATH=$HOME/.config/yarn/global/node_modules/.bin:$PATH
export PATH=$HOME/bin:$PATH
export PATH=$HOME/Library/Android/sdk/platform-tools:$PATH
export PATH=$JAVA_HOME/bin:$PATH


if which ruby >/dev/null && which gem >/dev/null; then
  PATH="$(ruby -r rubygems -e 'puts Gem.user_dir')/bin:$PATH"
fi

# Reset the theme. Pure is used as a theme manager.
ZSH_THEME=""

function load_nvm() {
  [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"
  [ -s "$NVM_DIR/bash_completion" ] && . "$NVM_DIR/bask_completion"
}

# Source the different directories
source $ZSH/custom/plugins/zsh-async/async.zsh
source $ZSH/custom/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source $ZSH/custom/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $ZSH/oh-my-zsh.sh
source $HOME/.zsh/aliases.zsh

# Initialize worker
async_start_worker nvm_worker -n
async_register_callback nvm_worker load_nvm
async_job nvm_worker sleep 0.1


plugins=(composer laravel5 sudo zsh-autosuggestions zsh-syntax-highlighting)

autoload -U promptinit; promptinit
prompt pure
