# Edit config files
alias vrc='vim ~/.vimrc'
alias gvrc='vim ~/.gvimrc'
alias zrc='vim ~/.zshrc'
alias af='vim ~/.zsh/aliases.zsh'
alias vpf='vim ~/.vim/plugins.vim'
alias hosts='sudo vim /private/etc/hosts'

# Open snippets
alias phpsnip='mvim ~/.vim/snippets/php.snippets'

# Project & Web aliases
alias ph='cd ~/Development'
alias mfbm='cd ~/Development/pvmeijgaard/mobiliteitsfabriek-middleware'
alias mfba='cd ~/Development/pvmeijgaard/mobiliteitsfabriek-app'
alias simw='cd ~/Development/pvmeijgaard/SimpelWeb'
alias simb='cd ~/Development/pvmeijgaard/SimpelBll'
alias sima='cd ~/Development/pvmeijgaard/SimpelApp'
alias twd='cd ~/Development/artisans/topwind'

# Git
alias git:reset='git reset --hard; git clean -df'
alias git:prune='git checkout master; git branch | egrep -v "(^\*|master|develop)" | xargs git branch -D'

# NPM
alias npm:d='npm run dev'
alias npm:s='npm run serve'
alias npm:b='npm run build'

# Yarn
alias yarn:d='yarn run dev'
alias yarn:s='yarn run serve'
alias yarn:b='yarn run build'

# Laravel Artisan commands
alias art='php artisan'
alias art:tinker='php artisan tinker'
alias art:mr='php artisan migrate:refresh'
alias art:mrs='php artisan migrate:refresh --seed'
alias sail='bash vendor/bin/sail'

# PHPUnit
alias pu='clear && vendor/bin/phpunit'

# Vagrant
alias vu='vagrant up'
alias vd='vagrant destroy --force'
alias vr='vagrant reload'
alias vh='vagrant halt'
alias vs='vagrant ssh'

# Homestead
alias hm='vendor/bin/homestead make'

# Composer
alias cda='composer dump-autoload'

# ZSH-specific
alias update='. ~/.zsh/aliases.zsh'
