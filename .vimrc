set nocompatible	"The latest Vim settings/options"

so ~/.vim/plugins.vim

syntax enable

"--- Override keys ---"
set backspace=indent,eol,start

"--- Override setting ---"
set backupdir=~/.vim/backup_files//	"Set backup directory"
set directory=~/.vim/swap_files//	"Set swap directory"
set undodir=~/.vim/undo_files//		"Set undo directory"
set hidden
set autowriteall                        "Always save the file"
set complete=.,w,b,u 			"Set autocompletion"

"--- Visuals ---"
colorscheme nord
set number				"Enable line numbers"
set linespace=15			"Line height"
set t_Co=256				"Set colors for terminal"
set guifont=Cascadia_Code:h15		"Set font and size"
set guioptions-=e			"Remove the tabs"
set noerrorbells visualbell t_vb=	"Disable the bells"

"Remove the sidebars for mvim"
set guioptions-=l
set guioptions-=L
set guioptions-=r
set guioptions-=R

"Override visuals"
"Override the visuals for the vsplit"
hi vertsplit guifg=bg guibg=bg
hi TabLineFill guifg=bg guibg=bg
"--- Search ---"
set hlsearch		"Enable highlight search"
set incsearch		"Enable incremental search"

"--- Split ---"
set splitbelow
set splitright

nmap <C-J> <C-W><C-J>
nmap <C-K> <C-W><C-K>
nmap <C-H> <C-W><C-H>
nmap <C-L> <C-W><C-L>

"--- Mappings ---"
let mapleader = ','
nmap <Leader>ev :tabedit $MYVIMRC<cr> 
nmap <Leader>es :e ~/.vim/snippets/
nmap <Leader><space> :nohlsearch<cr>

"--- Plugins ---"
"=== PeepOpen ==="
"--- Mappings ---"
nmap <D-p> <Plug>PeepOpen

"=== CtrlP ==="
"--- Mappings ---"
nmap <D-r> :CtrlPBufTag<cr>
nmap <D-e> :CtrlPMRUFiles<cr>

"--- Overrides ---"
let g:ctrlp_custom_ignore = 'node_modules\DS_Store\|git'
let g:ctrlp_match_window = 'top,order:ttb,min:1,max:30,results:30'
" The Silver Searcher
if executable('ag')
  " Use ag over grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

  " ag is fast enough that CtrlP doesn't need to cache
  let g:ctrlp_use_caching = 0
endif

"=== NERDTree ==="
"--- Mappings ---"
nmap <D-1> :NERDTreeToggle<cr>
"--- Overrides ---"
let NERDTreeHijackNetrw = 0

"=== CTags ==="
"--- Mappings ---"
nmap <Leader>f :tag<space>

"=== PregReplace ==="
"--- Overrides ---"
set grepprg=ag						"We want to use Ag"
let g:grep_cmd_opts = '--line-numbers --noheading'

"=== vim-php-cs-fixer.vim ==="
"--- Mappings ---"
nnoremap <silent><leader>pd :call PhpCsFixerFixDirectory()<CR>
nnoremap <silent><leader>pf :call PhpCsFixerFixFile()<CR>

"--- Overrides ---"
let g:php_cs_fixer_level = "psr2"

"--- Language and Framework Specific ---"
"=== Laravel ==="
nmap <Leader>lr :e app/Http/routes.php<cr>
nmap <Leader>lc :e app/Http/Controllers<cr>
nmap <Leader>lm :e app<cr>
nmap <Leader>lv :e resources/views<cr>

"--- Auto-Commands ---"
"Auto source the Vimrc file on save.
augroup autosourcing
	autocmd!
	autocmd BufWritePost .vimrc source%
augroup END

function! IPhpInsertUse()
    call PhpInsertUse()
    call feedkeys('a',  'n')
endfunction
autocmd FileType php inoremap <Leader>n <Esc>:call IPhpInsertUse()<CR>
autocmd FileType php noremap <Leader>n :call PhpInsertUse()<CR>

function! IPhpExpandClass()
    call PhpExpandClass()
    call feedkeys('a', 'n')
endfunction
autocmd FileType php inoremap <Leader>nf <Esc>:call IPhpExpandClass()<CR>
autocmd FileType php noremap <Leader>nf :call PhpExpandClass()<CR>

"--- Frequently Used Keystrokes ---"
" - Press ctrl + ] to jump to the method or property the cursor is on
" - Press zz to instantly center the line where the cursor is located
" - Press ctrl + ^ to jump back to the previous selection
" - Press vi' to select everything between the single quotes
"   (You can change the single quote to you desired character)
" - Press di' to delete everything between the single quotes
" - Press ci' to change everything between the single quotes
" - Press gg to move to the top of the file
" - Press G to move to the end of the file
